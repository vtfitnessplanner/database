# Use the official MySQL image as the base image
FROM mysql:8.0

# Set environment variables for MySQL
ENV MYSQL_DATABASE=vtmealplandb
ENV MYSQL_ROOT_PASSWORD=password


COPY ./vtmealplan-ddl.sql /docker-entrypoint-initdb.d/
COPY ./vtmealplan-dml.sql /docker-entrypoint-initdb.d/

# Expose port 3306 to allow communication to/from server
EXPOSE 3306
