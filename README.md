# MySQL Database for VT Meal Planner Application

## Database Introduction
We are hosting our database on VT's private Kubernetes cluster, cloud.cs.vt.edu. On the cloud, we have a project titled `vtmealplanner` and a namespace titled `vtmealplan`. This namespaces is hosting all of our database components for thefull-stack web application.

## Steps to Install MySQL Server
Since we configured the host environment namespace with resource quotas and storage provisioning, installing MySQL is relatively straightforward. For our namespace environment, each container is defaulted to a certain amount of resources,and in our case, 0.5 CPU.

First, navigate to the Apps and Marketplace tab, and scroll down to MySQL. Next, apply the values.yaml file configuration we have in our repo. This modified values.yaml file employs some security configurations necessary for network communication.

After deploying, this should be the final output of the below command:
```
kubectl get all -n vtmealplan
```
Output of above command:
```
NAME                       READY   STATUS    RESTARTS   AGE
pod/vtmealplandb-mysql-0   1/1     Running   1          4m34s

NAME                                  TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
service/vtmealplandb-mysql            ClusterIP   10.43.69.196   <none>        3306/TCP   4m34s
service/vtmealplandb-mysql-headless   ClusterIP   None           <none>        3306/TCP   4m34s

NAME                                  READY   AGE
statefulset.apps/vtmealplandb-mysql   1/1     4m34s
```

## How to Connect
This approach is better as we can distribute our database for resiliency purposes, or keep things to one version of the truth when running backend API calls.

Set environment variable:
```
MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace vtmealplan vtmealplandb-mysql -o jsonpath="{.data.mysql-root-password}" | base64 -d)
```
Run a client pod IF ITS NOT ALREADY PRESENT:
```
kubectl run vtmealplandb-mysql-client --rm --tty -i --restart='Never' --image docker.io/bitnami/mysql:8.0.36-debian-11-r0 --namespace vtmealplan --env MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD --command -- bash
```
IF the client pod is present:
```
kubectl attach vtmealplandb-mysql-client -c vtmealplandb-mysql-client -i -t
```
login to MySQL via local dns service routing:
```
mysql -h vtmealplandb-mysql.vtmealplan.svc.cluster.local -uroot -p"$MYSQL_ROOT_PASSWORD"
```
## How to Connect (ALTERNATIVE)
This command will allow you to log into the MySQL application host environment pod.
```
kubectl exec -it vtmealplandb-mysql-0 -n vtmealplan -- sh
```
Then,  to run mysql commands, view databases, tables:
```
mysql -uroot -p$MYSQL_ROOT_PASSWORD
```
Test if a MySQL command works:
```
show databases;
```

## Setup a Development Container
Instead of rebuilding your Python image everytime, you can simply run a development container for the MySQL database as a "mock production environment". This will allow you to develop backend code locally without building an image for thebackend code every single time. Here's how to create the development databse that most resembles our cloud.cs.vt.edu production environment:
```
docker build -t testdb:dev .
docker run -d -p 3306:3306 --name vtmealplandb testdb:dev
docker ps -a
docker exec -it vtmealplandb sh
mysql -uroot -ppassword
use vtmealplandb;
SELECT * FROM FitnessStat;
```

Now if you develop locally, you can use this database for development. Connecting to 127.0.0.1 or localhost as your host connection. The -p 3306:3306 in the docker run command "port forwards" the container port to your local machine 3306 port, so you can access your container service at "localhost:3306" instead of "containerIP:3306" on the container level.

## Cross Application Communication
If we are hosting our application through the cloud.cs.vt.edu instance, application networking should be implementable. For example, if we host our backend service on Kubernetes, we can set up the communication with the served database instance much easier than it not being on the Kubernetes cluster.
