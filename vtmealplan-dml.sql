INSERT INTO User (userFName, userLName, userEmail, userDOB, userGender, userAuthToken)
VALUES ('John', 'Doe', 'john.doe@example.com', '1990-01-15', 'Male', 'xyz123token');

INSERT INTO WorkoutPlan (workoutType, workoutCalorieExpect)
VALUES ('Cardio', 2500),
       ('Bulking', 4000),
       ('Aerobics', 1700);

INSERT INTO FitnessStat (fitstat_height_inches, fitstat_weight_lbs, workoutPlanID, userID)
VALUES (68.5, 150.0, 1, 1);