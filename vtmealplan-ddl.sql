CREATE DATABASE IF NOT EXISTS vtmealplandb;
USE vtmealplandb;

-- CREATE TABLE Loc (
-- 	locationID int NOT NULL AUTO_INCREMENT UNIQUE,
-- 	locationName varchar(255) DEFAULT NULL,
-- 	locationPosition varchar(255) DEFAULT NULL,
--     PRIMARY KEY (locationID)
-- );

CREATE TABLE User (
    userID int NOT NULL AUTO_INCREMENT UNIQUE,
    userFName varchar(255) DEFAULT NULL,
    userLName varchar(255) DEFAULT NULL,
    userEmail varchar(255) DEFAULT NULL UNIQUE,
    userDOB date DEFAULT NULL,
    userGender varchar(255) DEFAULT NULL,
    userAuthToken varchar(255) DEFAULT NULL,
    PRIMARY KEY (userID)
);

CREATE TABLE WorkoutPlan (
    workoutPlanID int NOT NULL AUTO_INCREMENT UNIQUE,
    workoutType varchar(255) DEFAULT NULL,
    workoutCalorieExpect int DEFAULT NULL,
    PRIMARY KEY (workoutPlanID)
);

CREATE TABLE Food (
	foodID int NOT NULL AUTO_INCREMENT UNIQUE,
    foodName varchar(255) DEFAULT NULL,
    foodCalorie int DEFAULT NULL,
    foodFat decimal(4,1) DEFAULT NULL,
    foodCholesterol decimal(4,1) DEFAULT NULL,
    foodSodium decimal(4,1) DEFAULT NULL,
    foodCarb decimal(4,1) DEFAULT NULL,
    foodSugar decimal(4,1) DEFAULT NULL,
    foodProtein decimal(4,1) DEFAULT NULL,
    foodServingTime varchar(255) DEFAULT NULL,
    foodLocation varchar(255),
    PRIMARY KEY (foodID)
);

CREATE TABLE FoodDate (
    foodDateID int NOT NULL AUTO_INCREMENT UNIQUE,
    foodDateDate date DEFAULT NULL,
    foodID int,
    PRIMARY KEY (foodDateID),
    FOREIGN KEY (foodID) REFERENCES Food (foodID)
);

CREATE TABLE Meal (
    mealID int NOT NULL AUTO_INCREMENT UNIQUE,
    foodID int,
    userID int,
    foodDateID int,
    PRIMARY KEY (mealID),
    FOREIGN KEY (foodID) REFERENCES Food (foodID),
    FOREIGN KEY (userID) REFERENCES User (userID),
    FOREIGN KEY (foodDateID) REFERENCES FoodDate (foodDateID)
);

CREATE TABLE FitnessStat (
    fitstatID int NOT NULL AUTO_INCREMENT UNIQUE,
    fitstat_height_inches decimal(4,1) DEFAULT NULL,
    fitstat_weight_lbs decimal(4,1) DEFAULT NULL,
    workoutPlanID int,
    userID int,
    PRIMARY KEY (fitstatID),
    FOREIGN KEY (workoutPlanID) REFERENCES WorkoutPlan (workoutPlanID),
    FOREIGN KEY (userID) REFERENCES User (userID)
);