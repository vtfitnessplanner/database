# Helpful Queries for Database

### Get Number of Duplicates in Food Table
```
SELECT foodName, COUNT(*)
FROM Food
GROUP BY foodName
HAVING COUNT(*) > 1;
```

### Verify "Different" Duplicate Entries

* ? is a foodName placeholder.

```
SELECT * FROM Food WHERE foodName = ?;
```

### Get All Dates of Food Item

* ? is a foodID placeholder.

```
SELECT * FROM FoodDate WHERE foodID = ?;
```

### Delete All Data X Days Old

* X is an integer for amount of days placeholder.

```
DELETE FROM FoodDate
WHERE DATEDIFF(NOW(), foodDateDate) = X;
```

```
DELETE FROM FoodDate
WHERE DATEDIFF(STR_TO_DATE('YYYY-MM-DD', '%Y-%m-%d'), your_date_column) = X;
```

### Delete Food Data No Longer in FoodDate Table

```
DELETE FROM Food
WHERE NOT EXISTS (
    SELECT * FROM FoodDate WHERE FoodDate.foodID = Food.foodID
);
```